# Flas

Internet radio broadcast searcher and player for Ubuntu Touch.

Continuation of the work done by Szymon Waliczek (thanks!).

[![OpenStore](https://open-store.io/badges/en_US.svg)](https://open-store.io/app/flas.cibersheep)

## Forked from

https://github.com/majster-pl/flas/tree/master/flas

## Translations

Now, thanks to Malte, we have a new [weblate for translations](https://translate-ut.org/projects/flas/flas/) 
New translations should be done using that link (if you started already an offline, contact me or MR with WIP: in front of the title. Thank you.

## License

Copyright (C) 2019  Joan CiberSheep

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
